class Nodo{
    constructor(nodoPadre = null, nombre, valor) {
        this.nombre = nombre;
        this.valor = valor;
        if(nodoPadre == null)
        {
            this.nivel = 0;
            this.padre = nodoPadre;
        }
        else
            this.nivel = nodoPadre.nivel + 1;
    }
}
